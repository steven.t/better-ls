package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/levenlabs/go-llog"
)

func main() {
	a := flag.Bool("a", true, "do not ignore entries with . ")
	all := flag.Bool("all", true, "do not ignore entries with . ")
	flag.Parse()

	if *a || *all {
		fmt.Printf("\tHidden files will be shown\n")
	}

	files, err := os.ReadDir(".")
	if err != nil {
		fmt.Errorf("Could not read directory: %s", err)
	}

	for _, file := range files {
		f, err := file.Info()
		noError(err, fmt.Sprintf("unable to read file info\n"))
		name := f.Name()
		if f.IsDir() {
			name = f.Name() + "/"
		}
		fmt.Printf("%s\n", name)
	}
}

func noError(err error, message string) {
	if err != nil {
		llog.Fatal(message)
	}
}
