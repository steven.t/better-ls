module gitlab.com/steven.turturo/better-ls

go 1.18

require github.com/levenlabs/go-llog v1.0.0

require github.com/levenlabs/errctx v1.0.0 // indirect
